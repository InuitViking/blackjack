/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Initiate = __webpack_require__(1);

var _Initiate2 = _interopRequireDefault(_Initiate);

var _Start = __webpack_require__(4);

var _Start2 = _interopRequireDefault(_Start);

var _Game = __webpack_require__(5);

var _Game2 = _interopRequireDefault(_Game);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/*
 * A Program class to make it as object oriented as possible
*/
var Program = function () {
	function Program() {
		_classCallCheck(this, Program);
	}

	_createClass(Program, [{
		key: 'Main',
		value: function Main() {
			// I wanted to put these two into a constructor, but that resulted in unexpected behaviour
			var deck = new _Initiate2.default();
			deck.Shuffle();
			// ################## START ##################
			var start = new _Start2.default(100);
			// Listen for click event on "bet"
			document.getElementById("bet").onclick = function () {
				start.OnClickBet();
			};

			// ################## GAME ##################
			var game = new _Game2.default(deck);
			game.DisplayDealerHand(false);
			game.DisplayPlayerHand();

			// if(game.DealerHand.GetCard(0) == "ace"){
			// 	document.getElementById("insuranceBtn").removeAttribute(disabled);
			// }

			// ################## ONCLICKS ##################
			// Check if user wants one more card
			document.getElementById("hit").onclick = function () {
				var status = game.HitMe(deck);
				// If user goes above 21, display dealer's hand
				if (status === false) {
					alert("Bust! You scored above 21!");
					game.DisplayDealerHand(true);
				} else if (status === "6cc") {
					alert("You got a 6-Card Charlie!");
					game.DisplayDealerHand(true);
				}
			};

			// Check on if user ends their turn and display if they've won or not with an alert box
			document.getElementById("stand").onclick = function () {
				var status = game.Stand(deck);
				if (status === "win") {
					start.Win();
					document.getElementById('insuranceBtn').setAttribute("disabled", true);
					alert("You win!");
				} else if (status === "push") {
					start.Push();
					document.getElementById('insuranceBtn').setAttribute("disabled", true);
					alert("It's a push!");
					start.WinInsurance();
				} else if (status === "natural") {
					start.Natural();
					document.getElementById('insuranceBtn').setAttribute("disabled", true);
					alert("BLACKJACK! YOU WIN!");
				} else {
					start.WinInsurance();
					document.getElementById('insuranceBtn').setAttribute("disabled", true);
					alert("You lost!");
				}
			};

			// Double down for extra win!
			// Or so you hope.
			document.getElementById("doubleDown").onclick = function () {
				start.DoubleDown();
				document.getElementById('insuranceBtn').setAttribute("disabled", true);
				var status = game.DoubleDown(deck);
				if (status === "win") {
					start.Win();
					document.getElementById('insuranceBtn').setAttribute("disabled", true);
					alert("You win!");
				} else if (status === "push") {
					start.Push();
					document.getElementById('insuranceBtn').setAttribute("disabled", true);
					alert("It's a push!");
				} else if (status === "natural") {
					// this thing won't happen, as with a double down, you'll never get a natural
					start.Natural();
					document.getElementById('insuranceBtn').setAttribute("disabled", true);
					alert("BLACKJACK! YOU WIN!");
				} else {
					document.getElementById('insuranceBtn').setAttribute("disabled", true);
					alert("You lost!");
				}
			};

			document.getElementById("insuranceBtn").onclick = function () {
				start.Insurance();
			};

			document.getElementById("surrender").onclick = function () {
				start.Surrender();
				alert("Half of bet was surrendered");
				document.getElementById("surrender").removeAttribute("disabled");
				document.getElementById('restart').click();
			};

			// I also wanted this one in a method for itself, but it needed a bit too many things, so I got lazy
			document.getElementById("restart").onclick = function () {
				document.getElementById("start").className = "shown";
				document.getElementById("game").className = "hidden";
				deck = new _Initiate2.default();
				deck.Shuffle();
				game = new _Game2.default(deck);
				game.DisplayDealerHand(false);
				game.DisplayPlayerHand();
				start.Ins = 0;
				document.getElementById("insurance").innerHTML = start.Ins;
				document.getElementById("hit").removeAttribute("disabled");
				document.getElementById("stand").removeAttribute("disabled");
				document.getElementById("doubleDown").removeAttribute("disabled");
				document.getElementById("surrender").removeAttribute("disabled");
			};

			// ################## EVENT LISTENERS ##################
			// Disable the enter key to prevent accidentally submitting the form
			window.addEventListener('keydown', function (e) {
				if (e.keyIdentifier == 'U+000A' || e.keyIdentifier == 'Enter' || e.keyCode == 13) {
					if (e.target.nodeName == 'INPUT' && e.target.type == 'number') {
						e.preventDefault();
						return false;
					}
				}
			}, true);
		}
	}]);

	return Program;
}();

// Run the entire thing


var program = new Program();
program.Main();

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _CardDeck = __webpack_require__(2);

var _CardDeck2 = _interopRequireDefault(_CardDeck);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Initiate = function Initiate() {
	_classCallCheck(this, Initiate);

	var deck = new _CardDeck2.default();
	var colours = ["clubs", "diamonds", "hearts", "spades"];
	var variations = ["ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "knight", "queen", "king"];
	var values = ["ace", 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10];
	var unicodes = ["🃑", "🃒", "🃓", "🃔", "🃕", "🃖", "🃗", "🃘", "🃙", "🃚", "🃛", "🃝", "🃞", // All clubs
	"🃁", "🃂", "🃃", "🃄", "🃅", "🃆", "🃇", "🃈", "🃉", "🃊", "🃋", "🃍", "🃎", // All diamonds
	"🂱", "🂲", "🂳", "🂴", "🂵", "🂶", "🂷", "🂸", "🂹", "🂺", "🂻", "🂽", "🂾", // All hearts
	"🂡", "🂢", "🂣", "🂤", "🂥", "🂦", "🂧", "🂨", "🂩", "🂪", "🂫", "🂭", "🂮" // All spades
	];
	// generate
	deck.Generate(colours, variations, values);

	// Give the cards something to show
	for (var i = 0; i < deck.Get().length; i++) {
		deck.GetCard(i).SetUnicode(unicodes[i]);
	}
	return deck;
};

exports.default = Initiate;

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Card = __webpack_require__(3);

var _Card2 = _interopRequireDefault(_Card);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var CardDeck = function () {
	// constructor
	function CardDeck() {
		_classCallCheck(this, CardDeck);

		this.Deck = [];
	}

	// Getters and setters


	_createClass(CardDeck, [{
		key: 'Get',
		value: function Get() {
			return this.Deck;
		}
	}, {
		key: 'GetCard',
		value: function GetCard(i) {
			return this.Deck[i];
		}

		// methods
		// Generates the Deck

	}, {
		key: 'Generate',
		value: function Generate(colours, variants, values) {
			if (this.Deck.length > 0) {
				this.Deck = [];
			}
			for (var c = 0; c < colours.length; c++) {
				for (var v = 0; v < variants.length; v++) {
					this.Deck.push(new _Card2.default(colours[c], variants[v], values[v]));
				}
			}
		}

		// shuffles the Deck

	}, {
		key: 'Shuffle',
		value: function Shuffle() {
			for (var i = this.Deck.length - 1; i > 0; i--) {
				var j = Math.floor(Math.random() * (i + 1));
				var _ref = [this.Deck[j], this.Deck[i]];
				this.Deck[i] = _ref[0];
				this.Deck[j] = _ref[1];
			}
		}
	}, {
		key: 'NextCard',
		value: function NextCard() {
			return this.Deck.shift();
		}
	}]);

	return CardDeck;
}();

exports.default = CardDeck;

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Card = function () {
	// Constructors
	function Card(colour, variant, value) {
		_classCallCheck(this, Card);

		this.Colour = colour;
		this.Variant = variant;
		this.Value = value;
	}

	// Getters and setters


	_createClass(Card, [{
		key: "GetColour",
		value: function GetColour() {
			return this.Colour;
		}
	}, {
		key: "SetColour",
		value: function SetColour(colour) {
			this.Colour = colour;
		}
	}, {
		key: "GetVariant",
		value: function GetVariant() {
			return this.Variant;
		}
	}, {
		key: "SetVariant",
		value: function SetVariant(variant) {
			this.Variant = variant;
		}
	}, {
		key: "GetUnicode",
		value: function GetUnicode() {
			return this.Unicode;
		}
	}, {
		key: "SetUnicode",
		value: function SetUnicode(unicodeChar) {
			this.Unicode = unicodeChar;
		}
	}, {
		key: "GetValue",
		value: function GetValue() {
			return this.Value;
		}
	}, {
		key: "Get",
		value: function Get() {
			return this.Colour + " " + this.Variant;
		}
	}]);

	return Card;
}();

exports.default = Card;

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Start = function () {
	function Start(tokens) {
		_classCallCheck(this, Start);

		this.Tokens = tokens;
		this.Betted = 0;
		this.Ins = 0;
		document.getElementById("start").className = "shown";
		document.getElementById("insurance").innerHTML = this.Ins;
		var tokensElements = document.getElementsByClassName('tokens');
		for (var i = 0; i < tokensElements.length; i++) {
			tokensElements[i].innerHTML = this.Tokens;
		}
	}

	_createClass(Start, [{
		key: "GetTokens",
		value: function GetTokens() {
			return this.Tokens;
		}
	}, {
		key: "SetTokens",
		value: function SetTokens(tokens) {
			this.Tokens = tokens;
		}
	}, {
		key: "UseTokens",
		value: function UseTokens(tokens) {
			this.Tokens = this.Tokens - tokens;
			this.Betted = tokens;
		}
	}, {
		key: "Bet",
		value: function Bet(tokens) {
			this.Tokens = this.Tokens - tokens;
			this.Betted = tokens;
		}
	}, {
		key: "Natural",
		value: function Natural() {
			this.Betted = this.Betted * 2.5;
			this.Tokens = this.Tokens + this.Betted;
			this.Betted = 0;
			var tokensElements = document.getElementsByClassName('tokens');
			document.getElementById('insuranceBtn').setAttribute("disabled", true);
			for (var i = 0; i < tokensElements.length; i++) {
				tokensElements[i].innerHTML = this.Tokens.toFixed(2);
			}
		}
	}, {
		key: "Win",
		value: function Win() {
			this.Betted = this.Betted * 2;
			this.Tokens = this.Tokens + this.Betted;
			this.Betted = 0;
			var tokensElements = document.getElementsByClassName('tokens');
			document.getElementById('insuranceBtn').setAttribute("disabled", true);
			for (var i = 0; i < tokensElements.length; i++) {
				tokensElements[i].innerHTML = this.Tokens.toFixed(2);
			}
		}
	}, {
		key: "Push",
		value: function Push() {
			this.Tokens = this.Tokens + this.Betted;
			this.Betted = 0;
			var tokensElements = document.getElementsByClassName('tokens');
			document.getElementById('insuranceBtn').setAttribute("disabled", true);
			for (var i = 0; i < tokensElements.length; i++) {
				tokensElements[i].innerHTML = this.Tokens.toFixed(2);
			}
		}
	}, {
		key: "GetBetted",
		value: function GetBetted() {
			return this.Betted;
		}
	}, {
		key: "OnClickBet",
		value: function OnClickBet() {
			var bettedTokensInput = parseFloat(document.getElementById("bettedTokensInput").value);
			if (!isNaN(bettedTokensInput)) {
				if (this.Tokens <= 0) {
					document.location.replace('http://i.qkme.me/3riad2.jpg');
				} else if (bettedTokensInput > this.Tokens) {
					alert("You can't bet more tokens than you already have!");
				} else if (bettedTokensInput === 0) {
					document.location.replace('https://i.kym-cdn.com/entries/icons/original/000/026/270/actually.jpg');
				} else {
					this.Betted = bettedTokensInput;
					this.Tokens = this.Tokens - this.Betted;
					document.getElementById("start").className = "hidden";
					document.getElementById("game").className = "shown";
					document.getElementById("bettedTokens").innerHTML = this.Betted.toFixed(2);
					var tokensElements = document.getElementsByClassName('tokens');
					for (var i = 0; i < tokensElements.length; i++) {
						tokensElements[i].innerHTML = this.Tokens.toFixed(2);
					}
				}
			} else {
				alert("That wasn't a number!");
			}
		}
	}, {
		key: "DoubleDown",
		value: function DoubleDown() {
			this.Tokens = this.Tokens - this.Betted;
			this.Betted = this.Betted * 2;
			var tokensElements = document.getElementsByClassName('tokens');
			document.getElementById('bettedTokens').innerHTML = this.Betted.toFixed(2);
			document.getElementById('insuranceBtn').setAttribute("disabled", true);
			for (var i = 0; i < tokensElements.length; i++) {
				tokensElements[i].innerHTML = this.Tokens;
			}
		}

		/*
   * Forfeit the game; Start class takes care of salvanging half of the original bet back.
  */

	}, {
		key: "Surrender",
		value: function Surrender() {
			this.Tokens = this.Tokens + this.Betted / 2;
			this.Betted = 0;
			this.Ins = 0;
			var tokensElements = document.getElementsByClassName('tokens');
			document.getElementById('bettedTokens').innerHTML = this.Betted.toFixed(2);
			document.getElementById('insuranceBtn').setAttribute("disabled", true);
			for (var i = 0; i < tokensElements.length; i++) {
				tokensElements[i].innerHTML = this.Tokens;
			}
		}

		// insurance takes the equivalent of 50% of the bet, and uses that as the insurance so you can win some of the money back.
		// The dealer *must* have an ace shown, and the player wins the insurance bet (2:1, that is the insurance bet * 2) if the
		// dealer has a natural 21.

	}, {
		key: "Insurance",
		value: function Insurance() {
			this.Tokens = this.Tokens - this.Betted / 2;
			this.Ins = this.Betted / 2;
			document.getElementById('insurance').innerHTML = this.Ins.toFixed(2);
			document.getElementById('insuranceBtn').setAttribute("disabled", true);
			var tokensElements = document.getElementsByClassName('tokens');
			document.getElementById('bettedTokens').innerHTML = this.Betted.toFixed(2);
			for (var i = 0; i < tokensElements.length; i++) {
				tokensElements[i].innerHTML = this.Tokens;
			}
		}
	}, {
		key: "WinInsurance",
		value: function WinInsurance() {
			this.Tokens = this.Tokens + this.Ins * 2;
			this.Betted = 0;
			this.Ins = 0;
			var tokensElements = document.getElementsByClassName('tokens');
			document.getElementById('insuranceBtn').setAttribute("disabled", true);
			for (var i = 0; i < tokensElements.length; i++) {
				tokensElements[i].innerHTML = this.Tokens;
			}
		}
	}]);

	return Start;
}();

exports.default = Start;

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Hand = __webpack_require__(6);

var _Hand2 = _interopRequireDefault(_Hand);

var _Exceptions = __webpack_require__(7);

var _Exceptions2 = _interopRequireDefault(_Exceptions);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// Takes care of most game logic
var Game = function () {
	function Game(deck) {
		_classCallCheck(this, Game);

		// give cards
		this.DealerHand = new _Hand2.default();
		this.PlayerHand = new _Hand2.default();
		for (var i = 0; i < 2; i++) {
			var dealerCard = deck.NextCard();
			var playerCard = deck.NextCard();
			this.DealerHand.Add(dealerCard);
			this.PlayerHand.Add(playerCard);
		}
	}

	// Returns the dealers hand


	_createClass(Game, [{
		key: 'GetDealerHand',
		value: function GetDealerHand() {
			return this.DealerHand.Get();
		}

		// Returns the players hand

	}, {
		key: 'GetPlayerHand',
		value: function GetPlayerHand() {
			return this.PlayerHand.Get();
		}

		/*
   * Displays the dealer's hand,
   * Depending on the trueOrFalse variable, this displays either only the first card (false) or all cards (true).
   * It also shows the score, depending on trueOrFalse, this displays the score value of the first card,
   * or it displays the total score.
  */

	}, {
		key: 'DisplayDealerHand',
		value: function DisplayDealerHand(trueOrFalse) {
			try {
				// check if trueOrFalse is defined
				if (trueOrFalse === null || trueOrFalse === undefined) {
					throw new _Exceptions2.default("DisplayDealerHand; trueOrFalse is" + trueOrFalse);
				}
				// Check if dealerHand exists
				if (document.getElementById("dealerHand") === null) {
					throw new _Exceptions2.default("Element with ID \"dealerHand\" does not exist. Please create it before using the DisplayDealerHand method.");
				}
				// Reset html
				var html = '';
				document.getElementById("dealerHand").innerHTML = html;
				// document.getElementById("dealerHand").innerHTML += `<p>Dealer's Hand: <span id="dealerScore"></span></p>`;
				// Divs to contains cards
				for (var i = 0; i < this.DealerHand.Get().length; i++) {
					html += '<div class="dealerCard"></div>';
				}
				// add that to the html
				document.getElementById("dealerHand").innerHTML += html;

				// get "dealerCard" elements
				var dealerCards = document.getElementsByClassName("dealerCard");

				for (var _i = 0; _i < this.DealerHand.Get().length; _i++) {
					// if trueorfalse is false
					if (trueOrFalse == false) {
						// if i is 0
						if (_i === 0) {
							// then display the first card
							dealerCards[_i].className += " " + this.DealerHand.GetCard(_i).GetColour();
							dealerCards[_i].innerHTML = "<span>" + this.DealerHand.GetCard(_i).Unicode + "</span>";
						} else {
							// else display the back
							dealerCards[_i].className += " back";
							dealerCards[_i].innerHTML = "<span>🂠</span>";
						}
					} else {
						// else just show all cards
						dealerCards[_i].className += " " + this.DealerHand.GetCard(_i).GetColour();
						dealerCards[_i].innerHTML = "<span>" + this.DealerHand.GetCard(_i).Unicode + "</span>";
					}
				}
				if (trueOrFalse == false) {
					// show the value of the first card
					var firstCard = this.DealerHand.Hand[0].GetValue();
					if (firstCard === "ace") {
						document.getElementById("dealerScore").innerHTML = "1 or 11";
						document.getElementById("insuranceBtn").removeAttribute("disabled");
					} else {
						document.getElementById("dealerScore").innerHTML = this.DealerHand.Hand[0].GetValue();
					}
				} else {
					// show total score
					document.getElementById("dealerScore").innerHTML = this.DealerHand.GetScore();
				}
			} catch (e) {
				console.error(e.message);
			}
		} // end of DisplayDealersHand

		/*
   * Displays the player's hand and score.
  */

	}, {
		key: 'DisplayPlayerHand',
		value: function DisplayPlayerHand() {
			try {
				// Check if playerHand exists
				if (document.getElementById("playerHand") === null) {
					throw new _Exceptions2.default("Element with ID \"playerHand\" does not exist. Please create it before using the DisplayDealerHand method.");
				}
				var html = '';
				document.getElementById("playerHand").innerHTML = html;
				// document.getElementById("playerHand").innerHTML = `<p>Your Hand: <span id="playerScore"></span></p>`;
				for (var i = 0; i < this.PlayerHand.Get().length; i++) {
					html += '<div class="playerCard"></div>';
				}
				document.getElementById("playerHand").innerHTML += html;
				var playerCards = document.getElementsByClassName("playerCard");
				for (var _i2 = 0; _i2 < this.PlayerHand.Get().length; _i2++) {
					playerCards[_i2].className += " " + this.PlayerHand.GetCard(_i2).GetColour();
					playerCards[_i2].innerHTML = "<span>" + this.PlayerHand.GetCard(_i2).Unicode + "</span>";
				}
				document.getElementById("playerScore").innerHTML = this.PlayerHand.GetScore();
			} catch (e) {
				console.error(e.message);
			}
		} // end of DisplayPlayerHand

		/*
   * Gives the player the next card from the specified deck.
   * This card is also removed from the deck.
  */

	}, {
		key: 'HitMe',
		value: function HitMe(deck) {
			try {
				// Check if playerHand exists
				if (document.getElementById("playerHand") === null) {
					throw new _Exceptions2.default("Element with ID \"playerHand\" does not exist. Please create it before using the HitMe method.");
				}
				// Check if playerScore exists
				if (document.getElementById("playerScore") === null) {
					throw new _Exceptions2.default("Element with ID \"playerScore\" does not exist. Please create it before using the HitMe method.");
				}
				// Get the next card and add it to hand
				var nextCard = deck.NextCard();
				this.PlayerHand.Add(nextCard);
				// reset shown cards
				var html = '';
				document.getElementById("playerHand").innerHTML = html;
				// this gets removed from earlier, so re-add it
				// document.getElementById("playerHand").innerHTML += `<p>Your Hand: <span id="playerScore"></span></p>`;
				// Display new amount of cards
				for (var i = 0; i < this.PlayerHand.Get().length; i++) {
					html += '<div class="playerCard"></div>';
				}
				document.getElementById("playerHand").innerHTML += html;
				// insert the necessary classes and unicode cards
				var playerCards = document.getElementsByClassName("playerCard");
				for (var _i3 = 0; _i3 < this.PlayerHand.Get().length; _i3++) {
					playerCards[_i3].className += " " + this.PlayerHand.GetCard(_i3).GetColour();
					playerCards[_i3].innerHTML = "<span>" + this.PlayerHand.GetCard(_i3).Unicode + "</span>";
				}
				// display the updated score
				document.getElementById("playerScore").innerHTML = this.PlayerHand.GetScore();

				// Check if score is above 21
				if (this.PlayerHand.GetScore() > 21) {
					document.getElementById("hit").setAttribute("disabled", true);
					document.getElementById("stand").setAttribute("disabled", true);
					document.getElementById("doubleDown").setAttribute("disabled", true);
					document.getElementById("surrender").setAttribute("disabled", true);
					return false;
				} else if (this.PlayerHand.Get().length === 6 && this.PlayerHand.GetScore() <= 21) {
					document.getElementById("hit").setAttribute("disabled", true);
					document.getElementById("stand").setAttribute("disabled", true);
					document.getElementById("doubleDown").setAttribute("disabled", true);
					document.getElementById("surrender").setAttribute("disabled", true);
					return this.Stand(deck);
				}
			} catch (e) {
				console.error(e.message);
			}
		} // end of hitme

		/*
   * Ends the game, and lets the dealer get more cards if needed.
   * This tries to get the dealer above the player's score and as close to 21 as possible
  */

	}, {
		key: 'Stand',
		value: function Stand(deck) {
			try {
				// Throw specific exceptions according if dealerHand, dealerScore, hit, or stand do not exist
				if (document.getElementById("dealerHand") === null) {
					throw new _Exceptions2.default("Element with ID \"dealerHand\" does not exist. Please create it before using the Stand method.");
				}
				if (document.getElementById("dealerScore") === null) {
					throw new _Exceptions2.default("Element with ID \"dealerScore\" does not exist. Please create it before using the Stand method.");
				}
				if (document.getElementById("hit") === null) {
					throw new _Exceptions2.default("Element with ID \"hit\" does not exist. Please create it before using the Stand method.");
				}
				if (document.getElementById("stand") === null) {
					throw new _Exceptions2.default("Element with ID \"stand\" does not exist. Please create it before using the Stand method.");
				}

				// Actual code here
				while (this.DealerHand.GetScore() <= this.PlayerHand.GetScore() && this.DealerHand.GetScore() < 17) {
					// Get the next card and add it to hand
					var nextCard = deck.NextCard();
					this.DealerHand.Add(nextCard);
					// reset shown cards
					var html = '';
					document.getElementById("dealerHand").innerHTML = html;
					// this gets removed from earlier, so re-add it
					// document.getElementById("dealerHand").innerHTML += `<p>Dealer's Hand: <span id="dealerScore"></span></p>`;
					// Display new amount of cards
					for (var i = 0; i < this.DealerHand.Get().length; i++) {
						html += '<div class="dealerCard"></div>';
					}
					document.getElementById("dealerHand").innerHTML += html;
					// insert the necessary classes and unicode cards
					var dealerCards = document.getElementsByClassName("dealerCard");
					for (var _i4 = 0; _i4 < this.DealerHand.Get().length; _i4++) {
						dealerCards[_i4].className += " " + this.DealerHand.GetCard(_i4).GetColour();
						dealerCards[_i4].innerHTML = "<span>" + this.DealerHand.GetCard(_i4).Unicode + "</span>";
					}
					// display the updated score
					document.getElementById("dealerScore").innerHTML = this.DealerHand.GetScore();
				}
				// check if player has won or not
				if (this.PlayerHand.GetScore() <= 21) {
					if (this.PlayerHand.GetScore() != this.DealerHand.GetScore()) {
						if (this.PlayerHand.Get().length === 6 && this.PlayerHand.GetScore() < 21 && this.DealerHand.Get().length !== 6) {
							document.getElementById("hit").setAttribute("disabled", true);
							document.getElementById("stand").setAttribute("disabled", true);
							document.getElementById("doubleDown").setAttribute("disabled", true);
							document.getElementById("surrender").setAttribute("disabled", true);
							this.DisplayDealerHand(true);
							return "6cc";
						} else if (this.PlayerHand.GetScore() === 21 && this.PlayerHand.Get().length === 2) {
							document.getElementById("hit").setAttribute("disabled", true);
							document.getElementById("stand").setAttribute("disabled", true);
							document.getElementById("doubleDown").setAttribute("disabled", true);
							document.getElementById("surrender").setAttribute("disabled", true);
							this.DisplayDealerHand(true);
							return "natural";
						} else if (this.DealerHand.GetScore() > 21) {
							document.getElementById("hit").setAttribute("disabled", true);
							document.getElementById("stand").setAttribute("disabled", true);
							document.getElementById("doubleDown").setAttribute("disabled", true);
							document.getElementById("surrender").setAttribute("disabled", true);
							this.DisplayDealerHand(true);
							return "win";
						} else if (this.DealerHand.GetScore() < this.PlayerHand.GetScore()) {
							document.getElementById("hit").setAttribute("disabled", true);
							document.getElementById("stand").setAttribute("disabled", true);
							document.getElementById("doubleDown").setAttribute("disabled", true);
							document.getElementById("surrender").setAttribute("disabled", true);
							this.DisplayDealerHand(true);
							return "win";
						} else if (this.DealerHand.GetScore() > this.PlayerHand.GetScore()) {
							document.getElementById("hit").setAttribute("disabled", true);
							document.getElementById("stand").setAttribute("disabled", true);
							document.getElementById("doubleDown").setAttribute("disabled", true);
							document.getElementById("surrender").setAttribute("disabled", true);
							this.DisplayDealerHand(true);
							return "lost";
						}
					} else {
						document.getElementById("hit").setAttribute("disabled", true);
						document.getElementById("stand").setAttribute("disabled", true);
						document.getElementById("doubleDown").setAttribute("disabled", true);
						document.getElementById("surrender").setAttribute("disabled", true);
						this.DisplayDealerHand(true);
						return "push";
					}
				}
			} catch (e) {
				console.error(e.message);
			}
		} // end of stand

		/*
   * Double down is the function that doubles the bet, draws a card, and immediately stands thereafter.
   * The method in here takes care of getting a card and immediately standing.
   * Start class takes care of doubling the bet.
  */

	}, {
		key: 'DoubleDown',
		value: function DoubleDown(deck) {
			this.HitMe(deck);
			return this.Stand(deck);
		}
	}]);

	return Game;
}();

exports.default = Game;

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Hand = function () {
	function Hand() {
		_classCallCheck(this, Hand);

		this.Hand = [];
	}

	_createClass(Hand, [{
		key: "Get",
		value: function Get() {
			return this.Hand;
		}
	}, {
		key: "GetCard",
		value: function GetCard(i) {
			return this.Hand[i];
		}
	}, {
		key: "Add",
		value: function Add(card) {
			this.Hand.push(card);
		}
	}, {
		key: "GetScore",
		value: function GetScore() {
			var score = 0;
			for (var i = 0; i < this.Hand.length; i++) {
				if (this.Hand[i].GetValue() !== "ace") {
					score = score + this.Hand[i].GetValue();
				}
			}
			for (var _i = 0; _i < this.Hand.length; _i++) {
				if (this.Hand[_i].GetValue() === "ace") {
					if (score + 11 > 21) {
						score = score + 1;
					} else if (score + 11 <= 21) {
						score = score + 11;
					} else {
						score = score + 1;
					}
				}
			}
			return score;
		}
	}]);

	return Hand;
}();

exports.default = Hand;

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/*
 * For specifying that an element doesn't exist
*/
var ElementUndefinedException = function (_Error) {
	_inherits(ElementUndefinedException, _Error);

	function ElementUndefinedException(message) {
		_classCallCheck(this, ElementUndefinedException);

		var _this = _possibleConstructorReturn(this, (ElementUndefinedException.__proto__ || Object.getPrototypeOf(ElementUndefinedException)).call(this));

		_this.message = "Error in either DisplayHand methods: " + message;
		return _this;
	}

	return ElementUndefinedException;
}(Error);

/*
 * For checking on wither trueOrFalse is null or undefined
*/


exports.default = ElementUndefinedException;

var TrueOrFalseException = exports.TrueOrFalseException = function (_Error2) {
	_inherits(TrueOrFalseException, _Error2);

	function TrueOrFalseException(message) {
		_classCallCheck(this, TrueOrFalseException);

		var _this2 = _possibleConstructorReturn(this, (TrueOrFalseException.__proto__ || Object.getPrototypeOf(TrueOrFalseException)).call(this));

		_this2.message = "Neither true or false was specified: " + message;
		return _this2;
	}

	return TrueOrFalseException;
}(Error);

/***/ })
/******/ ]);
//# sourceMappingURL=main.bundle.js.map