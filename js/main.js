import Initiate from './classes/Initiate';
import Start from './classes/Start';
import Game from './classes/Game';

/*
 * A Program class to make it as object oriented as possible
*/
class Program{
	Main(){
		// I wanted to put these two into a constructor, but that resulted in unexpected behaviour
		let deck = new Initiate();
		deck.Shuffle();
		// ################## START ##################
		let start = new Start(100);
		// Listen for click event on "bet"
		document.getElementById("bet").onclick = function(){start.OnClickBet();};

		// ################## GAME ##################
		let game = new Game(deck);
		game.DisplayDealerHand(false);
		game.DisplayPlayerHand();

		// if(game.DealerHand.GetCard(0) == "ace"){
		// 	document.getElementById("insuranceBtn").removeAttribute(disabled);
		// }

		// ################## ONCLICKS ##################
		// Check if user wants one more card
		document.getElementById("hit").onclick = function(){
			let status = game.HitMe(deck);
			// If user goes above 21, display dealer's hand
			if(status === false){
				alert("Bust! You scored above 21!");
				game.DisplayDealerHand(true);
			}else if(status === "6cc"){
				alert("You got a 6-Card Charlie!");
				game.DisplayDealerHand(true);
			}
		}

		// Check on if user ends their turn and display if they've won or not with an alert box
		document.getElementById("stand").onclick = function(){
			let status = game.Stand(deck);
			if(status === "win"){
				start.Win();
				document.getElementById('insuranceBtn').setAttribute("disabled", true);
				alert("You win!");
			}else if(status === "push"){
				start.Push();
				document.getElementById('insuranceBtn').setAttribute("disabled", true);
				alert("It's a push!");
				start.WinInsurance();
			}else if(status === "natural"){
				start.Natural();
				document.getElementById('insuranceBtn').setAttribute("disabled", true);
				alert("BLACKJACK! YOU WIN!");
			}else{
				start.WinInsurance();
				document.getElementById('insuranceBtn').setAttribute("disabled", true);
				alert("You lost!");
			}
		}

		// Double down for extra win!
		// Or so you hope.
		document.getElementById("doubleDown").onclick = function(){
			start.DoubleDown();
			document.getElementById('insuranceBtn').setAttribute("disabled", true);
			let status = game.DoubleDown(deck);
			if(status === "win"){
				start.Win();
				document.getElementById('insuranceBtn').setAttribute("disabled", true);
				alert("You win!");
			}else if(status === "push"){
				start.Push();
				document.getElementById('insuranceBtn').setAttribute("disabled", true);
				alert("It's a push!");
			}else if(status === "natural"){ // this thing won't happen, as with a double down, you'll never get a natural
				start.Natural();
				document.getElementById('insuranceBtn').setAttribute("disabled", true);
				alert("BLACKJACK! YOU WIN!");
			}else{
				document.getElementById('insuranceBtn').setAttribute("disabled", true);
				alert("You lost!");
			}
		}

		document.getElementById("insuranceBtn").onclick = function(){
			start.Insurance();
		}

		document.getElementById("surrender").onclick = function(){
			start.Surrender();
			alert("Half of bet was surrendered");
			document.getElementById("surrender").removeAttribute("disabled");
			document.getElementById('restart').click();
		}

		// I also wanted this one in a method for itself, but it needed a bit too many things, so I got lazy
		document.getElementById("restart").onclick = function(){
			document.getElementById("start").className = "shown";
			document.getElementById("game").className = "hidden";
			deck = new Initiate();
			deck.Shuffle();
			game = new Game(deck);
			game.DisplayDealerHand(false);
			game.DisplayPlayerHand();
			start.Ins = 0;
			document.getElementById("insurance").innerHTML = start.Ins;
			document.getElementById("hit").removeAttribute("disabled");
			document.getElementById("stand").removeAttribute("disabled");
			document.getElementById("doubleDown").removeAttribute("disabled");
			document.getElementById("surrender").removeAttribute("disabled");
		}

		// ################## EVENT LISTENERS ##################
		// Disable the enter key to prevent accidentally submitting the form
		window.addEventListener('keydown', function(e) {
			if (e.keyIdentifier == 'U+000A' || e.keyIdentifier == 'Enter' || e.keyCode == 13) {
				if (e.target.nodeName == 'INPUT' && e.target.type == 'number') {
					e.preventDefault();
					return false;
				}
			}
		}, true);

	}
}

// Run the entire thing
let program = new Program();
program.Main();
