import Card from './Card';
export default class CardDeck{
	// constructor
	constructor(){
		this.Deck = [];
	}

	// Getters and setters
	Get(){
		return this.Deck;
	}

	GetCard(i){
		return this.Deck[i];
	}

	// methods
	// Generates the Deck
	Generate(colours, variants, values){
		if(this.Deck.length > 0){
			this.Deck = [];
		}
		for(var c = 0; c < colours.length; c++){
			for(var v = 0; v < variants.length; v++){
				this.Deck.push(new Card(colours[c], variants[v], values[v]));
			}
		}
	}

	// shuffles the Deck
	Shuffle(){
		for (let i = this.Deck.length - 1; i > 0; i--) {
			const j = Math.floor(Math.random() * (i + 1));
			[this.Deck[i], this.Deck[j]] = [this.Deck[j], this.Deck[i]];
		}
	}

	NextCard(){
		return this.Deck.shift();
	}
}
