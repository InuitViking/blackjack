export default class Start{
	constructor(tokens){
		this.Tokens = tokens;
		this.Betted = 0;
		this.Ins = 0;
		document.getElementById("start").className = "shown";
		document.getElementById("insurance").innerHTML = this.Ins;
		let tokensElements = document.getElementsByClassName('tokens');
		for(let i = 0; i < tokensElements.length; i++){
			tokensElements[i].innerHTML = this.Tokens;
		}
	}

	GetTokens(){
		return this.Tokens;
	}

	SetTokens(tokens){
		this.Tokens = tokens;
	}

	UseTokens(tokens){
		this.Tokens = this.Tokens - tokens;
		this.Betted = tokens;
	}

	Bet(tokens){
		this.Tokens = this.Tokens - tokens;
		this.Betted = tokens;
	}

	Natural(){
		this.Betted = this.Betted * 2.5;
		this.Tokens = this.Tokens + this.Betted;
		this.Betted = 0;
		let tokensElements = document.getElementsByClassName('tokens');
		document.getElementById('insuranceBtn').setAttribute("disabled", true);
		for(let i = 0; i < tokensElements.length; i++){
			tokensElements[i].innerHTML = this.Tokens.toFixed(2);
		}
	}

	Win(){
		this.Betted = this.Betted * 2;
		this.Tokens = this.Tokens + this.Betted;
		this.Betted = 0;
		let tokensElements = document.getElementsByClassName('tokens');
		document.getElementById('insuranceBtn').setAttribute("disabled", true);
		for(let i = 0; i < tokensElements.length; i++){
			tokensElements[i].innerHTML = this.Tokens.toFixed(2);
		}
	}

	Push(){
		this.Tokens = this.Tokens + this.Betted;
		this.Betted = 0;
		let tokensElements = document.getElementsByClassName('tokens');
		document.getElementById('insuranceBtn').setAttribute("disabled", true);
		for(let i = 0; i < tokensElements.length; i++){
			tokensElements[i].innerHTML = this.Tokens.toFixed(2);
		}
	}

	GetBetted(){
		return this.Betted;
	}

	OnClickBet(){
		let bettedTokensInput = parseFloat(document.getElementById("bettedTokensInput").value)
		if(!isNaN(bettedTokensInput)){
			if(this.Tokens <= 0){
				document.location.replace('http://i.qkme.me/3riad2.jpg');
			}else if(bettedTokensInput > this.Tokens){
				alert("You can't bet more tokens than you already have!");
			}else if(bettedTokensInput === 0){
				document.location.replace('https://i.kym-cdn.com/entries/icons/original/000/026/270/actually.jpg');
			}else{
				this.Betted = bettedTokensInput;
				this.Tokens = this.Tokens - this.Betted;
				document.getElementById("start").className = "hidden";
				document.getElementById("game").className = "shown";
				document.getElementById("bettedTokens").innerHTML = this.Betted.toFixed(2);
				var tokensElements = document.getElementsByClassName('tokens');
				for(let i = 0; i < tokensElements.length; i++){
					tokensElements[i].innerHTML = this.Tokens.toFixed(2);
				}
			}
		}else{
			alert("That wasn't a number!");
		}
	}

	DoubleDown(){
		this.Tokens = this.Tokens - this.Betted;
		this.Betted = this.Betted * 2;
		let tokensElements = document.getElementsByClassName('tokens');
		document.getElementById('bettedTokens').innerHTML = this.Betted.toFixed(2);
		document.getElementById('insuranceBtn').setAttribute("disabled", true);
		for(let i = 0; i < tokensElements.length; i++){
			tokensElements[i].innerHTML = this.Tokens;
		}
	}

	/*
	 * Forfeit the game; Start class takes care of salvanging half of the original bet back.
	*/
	Surrender(){
		this.Tokens = this.Tokens + (this.Betted / 2);
		this.Betted = 0;
		this.Ins = 0;
		let tokensElements = document.getElementsByClassName('tokens');
		document.getElementById('bettedTokens').innerHTML = this.Betted.toFixed(2);
		document.getElementById('insuranceBtn').setAttribute("disabled", true);
		for(let i = 0; i < tokensElements.length; i++){
			tokensElements[i].innerHTML = this.Tokens;
		}
	}

	// insurance takes the equivalent of 50% of the bet, and uses that as the insurance so you can win some of the money back.
	// The dealer *must* have an ace shown, and the player wins the insurance bet (2:1, that is the insurance bet * 2) if the
	// dealer has a natural 21.
	Insurance(){
		this.Tokens = this.Tokens - (this.Betted / 2);
		this.Ins = this.Betted / 2;
		document.getElementById('insurance').innerHTML = this.Ins.toFixed(2);
		document.getElementById('insuranceBtn').setAttribute("disabled", true);
		let tokensElements = document.getElementsByClassName('tokens');
		document.getElementById('bettedTokens').innerHTML = this.Betted.toFixed(2);
		for(let i = 0; i < tokensElements.length; i++){
			tokensElements[i].innerHTML = this.Tokens;
		}
	}

	WinInsurance(){
		this.Tokens = this.Tokens + (this.Ins * 2);
		this.Betted = 0;
		this.Ins = 0;
		let tokensElements = document.getElementsByClassName('tokens');
		document.getElementById('insuranceBtn').setAttribute("disabled", true);
		for(let i = 0; i < tokensElements.length; i++){
			tokensElements[i].innerHTML = this.Tokens;
		}
	}
}
