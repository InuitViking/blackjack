import CardDeck from './CardDeck';
export default class Initiate{
	constructor(){
		let deck = new CardDeck();
		let colours = ["clubs", "diamonds", "hearts", "spades"];
		let variations = ["ace","2","3","4","5","6","7","8","9","10","knight","queen","king"];
		let values = ["ace", 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10];
		let unicodes = [
						"🃑", "🃒", "🃓", "🃔", "🃕", "🃖", "🃗", "🃘", "🃙", "🃚", "🃛", "🃝", "🃞",	// All clubs
						"🃁", "🃂", "🃃", "🃄", "🃅", "🃆", "🃇", "🃈", "🃉", "🃊", "🃋", "🃍", "🃎",	// All diamonds
						"🂱", "🂲", "🂳", "🂴", "🂵", "🂶", "🂷", "🂸", "🂹", "🂺", "🂻", "🂽", "🂾",	// All hearts
						"🂡", "🂢", "🂣", "🂤", "🂥", "🂦", "🂧", "🂨", "🂩", "🂪", "🂫", "🂭", "🂮"	// All spades
					];
		// generate
		deck.Generate(colours, variations, values);

		// Give the cards something to show
		for(let i = 0; i < deck.Get().length; i++){
			deck.GetCard(i).SetUnicode(unicodes[i]);
		}
		return deck;
	}
}
