import Hand from './Hand';
import ElementUndefinedException from './Exceptions';
import TrueOrFalseException from './Exceptions';

// Takes care of most game logic
export default class Game{
	constructor(deck){
		// give cards
		this.DealerHand = new Hand();
		this.PlayerHand = new Hand();
		for(let i = 0; i < 2; i++){
			let dealerCard = deck.NextCard();
			let playerCard = deck.NextCard();
			this.DealerHand.Add(dealerCard);
			this.PlayerHand.Add(playerCard);
		}
	}

	// Returns the dealers hand
	GetDealerHand(){
		return this.DealerHand.Get();
	}

	// Returns the players hand
	GetPlayerHand(){
		return this.PlayerHand.Get();
	}

	/*
	 * Displays the dealer's hand,
	 * Depending on the trueOrFalse variable, this displays either only the first card (false) or all cards (true).
	 * It also shows the score, depending on trueOrFalse, this displays the score value of the first card,
	 * or it displays the total score.
	*/
	DisplayDealerHand(trueOrFalse){
		try{
			// check if trueOrFalse is defined
			if(trueOrFalse === null || trueOrFalse === undefined){
				throw new TrueOrFalseException("DisplayDealerHand; trueOrFalse is" + trueOrFalse);
			}
			// Check if dealerHand exists
			if(document.getElementById("dealerHand") === null){
				throw new ElementUndefinedException("Element with ID \"dealerHand\" does not exist. Please create it before using the DisplayDealerHand method.");
			}
			// Reset html
			let html = ``;
			document.getElementById("dealerHand").innerHTML = html
			// document.getElementById("dealerHand").innerHTML += `<p>Dealer's Hand: <span id="dealerScore"></span></p>`;
			// Divs to contains cards
			for(let i = 0; i < this.DealerHand.Get().length; i++){
				html += `<div class="dealerCard"></div>`;
			}
			// add that to the html
			document.getElementById("dealerHand").innerHTML += html;

			// get "dealerCard" elements
			let dealerCards = document.getElementsByClassName("dealerCard");

			for(let i = 0; i < this.DealerHand.Get().length; i++){
				// if trueorfalse is false
				if(trueOrFalse == false){
					// if i is 0
					if(i === 0){
						// then display the first card
						dealerCards[i].className += " " + this.DealerHand.GetCard(i).GetColour();
						dealerCards[i].innerHTML = "<span>"+this.DealerHand.GetCard(i).Unicode+"</span>";
					}else{
						// else display the back
						dealerCards[i].className += " back";
						dealerCards[i].innerHTML = "<span>🂠</span>";
					}
				}else{ // else just show all cards
					dealerCards[i].className += " " + this.DealerHand.GetCard(i).GetColour();
					dealerCards[i].innerHTML = "<span>"+this.DealerHand.GetCard(i).Unicode+"</span>";
				}
			}
			if(trueOrFalse == false){// show the value of the first card
				let firstCard = this.DealerHand.Hand[0].GetValue();
				if(firstCard === "ace"){
					document.getElementById("dealerScore").innerHTML = "1 or 11";
					document.getElementById("insuranceBtn").removeAttribute("disabled");
				}else{
					document.getElementById("dealerScore").innerHTML = this.DealerHand.Hand[0].GetValue();
				}
			}else{// show total score
				document.getElementById("dealerScore").innerHTML = this.DealerHand.GetScore();
			}
		}catch(e){
			console.error(e.message);
		}
	}// end of DisplayDealersHand

	/*
	 * Displays the player's hand and score.
	*/
	DisplayPlayerHand(){
		try{
			// Check if playerHand exists
			if(document.getElementById("playerHand") === null){
				throw new ElementUndefinedException("Element with ID \"playerHand\" does not exist. Please create it before using the DisplayDealerHand method.");
			}
			let html = ``;
			document.getElementById("playerHand").innerHTML = html;
			// document.getElementById("playerHand").innerHTML = `<p>Your Hand: <span id="playerScore"></span></p>`;
			for(let i = 0; i < this.PlayerHand.Get().length; i++){
				html += `<div class="playerCard"></div>`;
			}
			document.getElementById("playerHand").innerHTML += html;
			let playerCards = document.getElementsByClassName("playerCard");
			for(let i = 0; i < this.PlayerHand.Get().length; i++){
				playerCards[i].className += " " + this.PlayerHand.GetCard(i).GetColour();
				playerCards[i].innerHTML = "<span>"+this.PlayerHand.GetCard(i).Unicode+"</span>";
			}
			document.getElementById("playerScore").innerHTML = this.PlayerHand.GetScore();
		}catch(e){
			console.error(e.message);
		}
	}// end of DisplayPlayerHand

	/*
	 * Gives the player the next card from the specified deck.
	 * This card is also removed from the deck.
	*/
	HitMe(deck){
		try{
			// Check if playerHand exists
			if(document.getElementById("playerHand") === null){
				throw new ElementUndefinedException("Element with ID \"playerHand\" does not exist. Please create it before using the HitMe method.");
			}
			// Check if playerScore exists
			if(document.getElementById("playerScore") === null){
				throw new ElementUndefinedException("Element with ID \"playerScore\" does not exist. Please create it before using the HitMe method.");
			}
			// Get the next card and add it to hand
			let nextCard = deck.NextCard();
			this.PlayerHand.Add(nextCard);
			// reset shown cards
			let html = ``;
			document.getElementById("playerHand").innerHTML = html;
			// this gets removed from earlier, so re-add it
			// document.getElementById("playerHand").innerHTML += `<p>Your Hand: <span id="playerScore"></span></p>`;
			// Display new amount of cards
			for(let i = 0; i < this.PlayerHand.Get().length; i++){
				html += `<div class="playerCard"></div>`;
			}
			document.getElementById("playerHand").innerHTML += html;
			// insert the necessary classes and unicode cards
			let playerCards = document.getElementsByClassName("playerCard");
			for(let i = 0; i < this.PlayerHand.Get().length; i++){
				playerCards[i].className += " " + this.PlayerHand.GetCard(i).GetColour();
				playerCards[i].innerHTML = "<span>"+this.PlayerHand.GetCard(i).Unicode+"</span>";
			}
			// display the updated score
			document.getElementById("playerScore").innerHTML = this.PlayerHand.GetScore();

			// Check if score is above 21
			if(this.PlayerHand.GetScore() > 21){
				document.getElementById("hit").setAttribute("disabled", true);
				document.getElementById("stand").setAttribute("disabled", true);
				document.getElementById("doubleDown").setAttribute("disabled", true);
				document.getElementById("surrender").setAttribute("disabled", true);
				return false;
			}else if(this.PlayerHand.Get().length === 6 && this.PlayerHand.GetScore() <= 21){
				document.getElementById("hit").setAttribute("disabled", true);
				document.getElementById("stand").setAttribute("disabled", true);
				document.getElementById("doubleDown").setAttribute("disabled", true);
				document.getElementById("surrender").setAttribute("disabled", true);
				return this.Stand(deck);
			}
		}catch(e){
			console.error(e.message);
		}
	}// end of hitme

	/*
	 * Ends the game, and lets the dealer get more cards if needed.
	 * This tries to get the dealer above the player's score and as close to 21 as possible
	*/
	Stand(deck){
		try{
			// Throw specific exceptions according if dealerHand, dealerScore, hit, or stand do not exist
			if(document.getElementById("dealerHand") === null){
				throw new ElementUndefinedException("Element with ID \"dealerHand\" does not exist. Please create it before using the Stand method.");
			}
			if(document.getElementById("dealerScore") === null){
				throw new ElementUndefinedException("Element with ID \"dealerScore\" does not exist. Please create it before using the Stand method.");
			}
			if(document.getElementById("hit") === null){
				throw new ElementUndefinedException("Element with ID \"hit\" does not exist. Please create it before using the Stand method.");
			}
			if(document.getElementById("stand") === null){
				throw new ElementUndefinedException("Element with ID \"stand\" does not exist. Please create it before using the Stand method.");
			}

			// Actual code here
			while(this.DealerHand.GetScore() <= this.PlayerHand.GetScore() && this.DealerHand.GetScore() < 17){
				// Get the next card and add it to hand
				let nextCard = deck.NextCard();
				this.DealerHand.Add(nextCard);
				// reset shown cards
				let html = ``;
				document.getElementById("dealerHand").innerHTML = html;
				// this gets removed from earlier, so re-add it
				// document.getElementById("dealerHand").innerHTML += `<p>Dealer's Hand: <span id="dealerScore"></span></p>`;
				// Display new amount of cards
				for(let i = 0; i < this.DealerHand.Get().length; i++){
					html += `<div class="dealerCard"></div>`;
				}
				document.getElementById("dealerHand").innerHTML += html;
				// insert the necessary classes and unicode cards
				let dealerCards = document.getElementsByClassName("dealerCard");
				for(let i = 0; i < this.DealerHand.Get().length; i++){
					dealerCards[i].className += " " + this.DealerHand.GetCard(i).GetColour();
					dealerCards[i].innerHTML = "<span>"+this.DealerHand.GetCard(i).Unicode+"</span>";
				}
				// display the updated score
				document.getElementById("dealerScore").innerHTML = this.DealerHand.GetScore();
			}
			// check if player has won or not
			if(this.PlayerHand.GetScore() <= 21){
				if(this.PlayerHand.GetScore() != this.DealerHand.GetScore()){
					if(this.PlayerHand.Get().length === 6 && this.PlayerHand.GetScore() < 21 && this.DealerHand.Get().length !== 6){
						document.getElementById("hit").setAttribute("disabled", true);
						document.getElementById("stand").setAttribute("disabled", true);
						document.getElementById("doubleDown").setAttribute("disabled", true);
						document.getElementById("surrender").setAttribute("disabled", true);
						this.DisplayDealerHand(true);
						return "6cc";
					}else if(this.PlayerHand.GetScore() === 21 && this.PlayerHand.Get().length === 2){
						document.getElementById("hit").setAttribute("disabled", true);
						document.getElementById("stand").setAttribute("disabled", true);
						document.getElementById("doubleDown").setAttribute("disabled", true);
						document.getElementById("surrender").setAttribute("disabled", true);
						this.DisplayDealerHand(true);
						return "natural";
					}else if(this.DealerHand.GetScore() > 21){
						document.getElementById("hit").setAttribute("disabled", true);
						document.getElementById("stand").setAttribute("disabled", true);
						document.getElementById("doubleDown").setAttribute("disabled", true);
						document.getElementById("surrender").setAttribute("disabled", true);
						this.DisplayDealerHand(true);
						return "win";
					}else if(this.DealerHand.GetScore() < this.PlayerHand.GetScore()){
						document.getElementById("hit").setAttribute("disabled", true);
						document.getElementById("stand").setAttribute("disabled", true);
						document.getElementById("doubleDown").setAttribute("disabled", true);
						document.getElementById("surrender").setAttribute("disabled", true);
						this.DisplayDealerHand(true);
						return "win";
					}else if(this.DealerHand.GetScore() > this.PlayerHand.GetScore()){
						document.getElementById("hit").setAttribute("disabled", true);
						document.getElementById("stand").setAttribute("disabled", true);
						document.getElementById("doubleDown").setAttribute("disabled", true);
						document.getElementById("surrender").setAttribute("disabled", true);
						this.DisplayDealerHand(true);
						return "lost";
					}
				}else{
					document.getElementById("hit").setAttribute("disabled", true);
					document.getElementById("stand").setAttribute("disabled", true);
					document.getElementById("doubleDown").setAttribute("disabled", true);
					document.getElementById("surrender").setAttribute("disabled", true);
					this.DisplayDealerHand(true);
					return "push";
				}
			}
		}catch(e){
			console.error(e.message);
		}
	}// end of stand

	/*
	 * Double down is the function that doubles the bet, draws a card, and immediately stands thereafter.
	 * The method in here takes care of getting a card and immediately standing.
	 * Start class takes care of doubling the bet.
	*/
	DoubleDown(deck){
		this.HitMe(deck);
		return this.Stand(deck);
	}
}
