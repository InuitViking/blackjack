export default class Card{
	// Constructors
	constructor(colour, variant, value){
		this.Colour = colour;
		this.Variant = variant;
		this.Value = value;
	}

	// Getters and setters
	GetColour(){
		return this.Colour;
	}

	SetColour(colour){
		this.Colour = colour;
	}

	GetVariant(){
		return this.Variant;
	}

	SetVariant(variant){
		this.Variant = variant;
	}

	GetUnicode(){
		return this.Unicode;
	}

	SetUnicode(unicodeChar){
		this.Unicode = unicodeChar;
	}

	GetValue(){
		return this.Value;
	}

	Get(){
		return this.Colour + " " + this.Variant;
	}
}
