export default class Hand{
	constructor(){
		this.Hand = [];
	}

	Get(){
		return this.Hand;
	}

	GetCard(i){
		return this.Hand[i];
	}

	Add(card){
		this.Hand.push(card);
	}

	GetScore(){
		let score = 0;
		for(let i = 0; i < this.Hand.length; i++){
			if(this.Hand[i].GetValue() !== "ace"){
				score = score + this.Hand[i].GetValue();
			}
		}
		for(let i = 0; i < this.Hand.length; i++){
			if(this.Hand[i].GetValue() === "ace"){
				if((score+11) > 21){
					score = score + 1;
				}else if((score+11) <= 21){
					score = score + 11;
				}else{
					score = score + 1;
				}
			}
		}
		return score
	}
}
