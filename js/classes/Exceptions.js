/*
 * For specifying that an element doesn't exist
*/
export default class ElementUndefinedException extends Error {
	constructor(message) {
		super();
		this.message = "Error in either DisplayHand methods: " + message;
	}
}

/*
 * For checking on wither trueOrFalse is null or undefined
*/
export class TrueOrFalseException extends Error{
	constructor(message){
		super();
		this.message = "Neither true or false was specified: " + message;
	}
}
