class CardDeck{
	// constructor
	constructor(){
		this.Deck = [];
	}

	// Getters and setters
	Get(){
		return this.Deck;
	}

	GetCard(i){
		return this.Deck[i].Get();
	}

	// methods
	// Generates the Deck
	Generate(colours, variants){
		if(this.Deck.length > 0){
			this.Deck = [];
		}
		for(var c = 0; c < colours.length; c++){
			for(var v = 0; v < variants.length; v++){
				this.Deck.push(new Card(colours[c], variants[v]))
			}
		}
	}

	// shuffles the Deck
	Shuffle(){
		for (let i = this.Deck.length - 1; i > 0; i--) {
			const j = Math.floor(Math.random() * (i + 1));
			[this.Deck[i], this.Deck[j]] = [this.Deck[j], this.Deck[i]];
		}
	}

	NextCard(){
		// this.Deck[0];
		return this.Deck.shift();
	}
}
class Card{
	// Constructors
	constructor(colour, variant){
		this.colour = colour;
		this.variant = variant;
	}

	// Getters and setters
	GetColour(){
		return this.colour;
	}

	SetColour(colour){
		this.colour = colour;
	}

	GetVariant(){
		return this.variant;
	}

	SetVariant(variant){
		this.variant = variant;
	}

	Get(){
		return this.colour + " " + this.variant;
	}
}
class Hand{
	constructor(){
		this.Hand = [];
	}

	Get(){
		return this.Hand;
	}

	GetCard(i){
		return this.Hand[i].Get();
	}

	Add(card){
		this.Hand.push(card);
	}
}
// initiate
let deck = new CardDeck();
let colours = ["hearts", "tiles", "clovers", "pikes"];
let variations = ["ace","2","3","4","5","6","7","8","9","10","knight","queen","king"];
var dealerHand = new Hand();
var playerHand = new Hand();

// generate
deck.Generate(colours, variations);

// shuffle
deck.Shuffle();